### Project Idea ###

Our project scope is related to fan control using Node MCU. Using a sensor which can measure temperature ranging from -55 degree celsius to 150 degree celsius, a user would input the maximum temperature values that our node MCU will use to toggle on/off the fan.
 


### Team Members ###
* Ayah Agha       201610263 
* Reem Sayed      201620262
* Sara Alnahdi    201620142
* Wadha Ahmed     201611286
* Amna Almazroui  201610611